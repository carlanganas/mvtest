<?php

    namespace App\Http\Controllers;

    use App\Models\Cart;
    use App\Models\Food;
    use Auth;
    use Illuminate\Contracts\Support\Renderable;
    use Illuminate\Http\Request;
    use Illuminate\Support\Str;

    class HomeController extends Controller
    {
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            $this->middleware('auth');
        }

        /**
         * Show the application dashboard.
         *
         * @return Renderable
         */
        public function index()
        {
            return view('welcome');
        }

        public function getUserData()
        {
            return Auth::user();
        }

        public function getFood()
        {

            $food = Food::all();
            return $food;
            $food = collect([]);
            $leng = 10;
            for ($i = 1; $i <= $leng; $i++) {
                $food->push([
                    'id' => $i,
                    'title' => Str::random(10),
                    'description' => Str::random(100),
                    'image' => 'https://images.pexels.com/photos/2983101/pexels-photo-2983101.jpeg?auto=compress&cs=tinysrgb&w=1600',

                ]);
            }
            return $food;
            /*

                */

        }
        public function saveCart(Request $request){
            $cart = $request->all();
            $cartModel = Cart::where('user_id',\Auth::user()->id)->first();
            if(empty($cartModel)) $cartModel = new Cart(['user_id'=>\Auth::user()->id]);
            $cartModel->user_id = \Auth::user()->id;
            $cartModel->cart = json_encode($cart);
            $cartModel->complete = 0;
            $cartModel->push();
            return $cart;



        }
        public function getCart(Request $request){
            $cart = Cart::where('user_id',\Auth::user()->id)->first();
            if(empty($cart)) $cart = new Cart();
            $cart = $cart->cart??[];
            return ($cart);

        }


    }
