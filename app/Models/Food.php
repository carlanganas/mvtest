<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Food
 * 
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Food extends Model
{
	protected $table = 'food';

	protected $fillable = [
		'title',
		'description',
		'image'
	];
}
