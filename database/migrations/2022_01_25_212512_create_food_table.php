<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateFoodTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            $faker = Faker\Factory::create();
            Schema::create('food', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->text('title');
                $table->longText('description');
                $table->text('image');
                $table->timestamps();
            });
            for ($i = 1; $i <= 20; $i++) {
                DB::table('food')->insert([
                    'id' => $i,
                    'title' => $faker->text(20),
                    'description' => $faker->realTextBetween(10, 200),
                    'image' => $faker->imageUrl($width = 640, $height = 480),
                ]);
            }


        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('food');
        }
    }
