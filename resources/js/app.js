/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueRouter from 'vue-router';
import store from './store';


window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('food-component', require('./components/FoodComponent.vue').default);
Vue.component('message-component', require('./components/Messages.vue').default);
Vue.component('user-component', require('./components/UserComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(VueRouter);
const home = '/home';
const routes = [
    {
        path: home,
        component: require('./components/WelcomeComponent.vue').default
    },
    {
        path: home+'/component',
        component: require('./components/RouteComponent.vue').default

    },
    {
        path: home+'/list',
        component: require('./components/FoodsComponent.vue').default

    },
    {
        path: home+'/*',
        component: require('./components/404Component.vue').default
    },
    // {path: '/index', component: require('./component/WelcomeComponent.vue)'.default}
]

const router= new VueRouter
({
    routes: routes,
    mode: 'history'
});


const app = new Vue({
//    el: '#app',
    store: store,
    router
}).$mount('#app');
