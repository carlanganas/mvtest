import Vue from 'vue';

window._ = require('lodash');
window.moment = require('moment');

try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap');
} catch (e) {
}


/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
import axios from 'axios';

window.axios = require('axios');
// jwt-token
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
let jwttoken = document.head.querySelector('meta[name="jwt-token"]');
if (token) {
    jwttoken = jwttoken.content;
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwttoken}`;
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    window.headers_token = {
        'X-CSRF-TOKEN': token.content,
        'Authorization': jwttoken,
    }
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

Vue.prototype.$http = axios;


Vue.prototype.$setStorage = function (name, obj) {
    localStorage.setItem(name, JSON.stringify(obj));
};
Vue.prototype.$getStorage = function (name) {
    return JSON.parse(localStorage.getItem(name));
};

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });
