import state from './state';

function readStorageData(variable, json = false) {
    let w = localStorage.getItem(variable)
    if (w === 'undefined') {
        w = null;
    }
    if (json === true) {
        w = JSON.parse(w)
    }
    return w;
}

export default {
    loadConfiguration(store) {
        state.config = readStorageData('config', true);
        if (state.items === undefined) state.items = [];
        if (state.user === undefined) state.user= {};
        if (state.cart === undefined) state.cart = [];
    },
    clearFormData() {
        state.form_data = {};
    },
    loadOffices(store) {
        if (state.form_data === undefined) state.form_data = {};
    },
}
