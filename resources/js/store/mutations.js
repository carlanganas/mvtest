import state from './state'

function writeLocal(variable, data) {
    if (data === undefined || data === 'undefined') {
        localStorage.removeItem(variable)
    } else {
        localStorage.setItem(variable, data)
    }
}

export default {
    setConfiguration(state, config) {
        writeLocal('config', JSON.stringify(config))
    },
    setItems(state, items) {
        state.items = (items === undefined) ? [] : items
    },
    setUser(state, user) {
        state.user = (user === undefined) ? {} : user
    },
    setCart(state, cart) {
        state.cart = (cart === undefined) ? [] : cart
    },
    addItemToCard(state, item) {
        state.cart = (state.cart === undefined) ? [] : state.cart
        if(item !== undefined) {
            state.cart.push(item)
        }
    },
    removeItemTocard(state,index){
        state.cart = (state.cart === undefined) ? [] : state.cart
        if(state.cart[index]) {
            state.cart.splice(index, 1);
        }

    }

}
