<ul id="accordionSidebar"
    class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion">

    <!-- Sidebar - Brand -->
    <router-link class="sidebar-brand d-flex align-items-center justify-content-center"
                 to="/home">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink">
            </i>
        </div>
        <div class="sidebar-brand-text mx-3">
            Admin
            <sup>1</sup>
        </div>
    </router-link>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <router-link class="nav-link"
                     to="/home/">
            <i class="fas fa-fw fa-tachometer-alt">
            </i>
            <span>
                Lista de comidas
            </span>
        </router-link>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Pedidos
    </div>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <router-link class="nav-link"
                     to="/home/list">
            <i class="fas fa-fw fa-tachometer-alt">
            </i>
            <span>
                Mi selección
            </span>
        </router-link>
    </li>

    {{--
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <router-link aria-controls="collapseTwo"
                     aria-expanded="true"
                     class="nav-link collapsed"
                     data-target="#collapseTwo"
                     data-toggle="collapse"
                     to="/home/list">
            <i class="fas fa-fw fa-cog">

            </i>
            <span>Mi lista</span>
        </router-link>
        <div id="collapseTwo"
             aria-labelledby="headingTwo"
             class="collapse"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Components:</h6>
                <router-link class="collapse-item"
                             to="/home">Buttons
                </router-link>
                <router-link class="collapse-item"
                             to="/home">
                    Cards
                </router-link>
            </div>
        </div>
    </li>


    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <router-link aria-controls="collapseUtilities"
                     aria-expanded="true"
                     class="nav-link collapsed"
                     data-target="#collapseUtilities"
                     data-toggle="collapse"
                     to="#">
            <i class="fas fa-fw fa-wrench">
            </i>
            <span>Utilities</span>
        </router-link>
        <div id="collapseUtilities"
             aria-labelledby="headingUtilities"
             class="collapse"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Utilities:</h6>
                <router-link class="collapse-item"
                             to="/home">Colors
                </router-link>
                <router-link class="collapse-item"
                             to="/home">Borders
                </router-link>
                <router-link class="collapse-item"
                             to="/home">Animations
                </router-link>
                <router-link class="collapse-item"
                             to="/home">Other
                </router-link>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Addons
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <router-link aria-controls="collapsePages"
                     aria-expanded="true"
                     class="nav-link collapsed"
                     data-target="#collapsePages"
                     data-toggle="collapse"
                     to="#">
            <i class="fas fa-fw fa-folder">
            </i>
            <span>Pages</span>
        </router-link>
        <div id="collapsePages"
             aria-labelledby="headingPages"
             class="collapse"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Login Screens:</h6>
                <router-link class="collapse-item"
                             to="login.html">Login
                </router-link>
                <router-link class="collapse-item"
                             to="register.html">Register
                </router-link>
                <router-link class="collapse-item"
                             to="forgot-password.html">Forgot Password
                </router-link>
                <div class="collapse-divider">
                </div>
                <h6 class="collapse-header">Other Pages:</h6>
                <router-link class="collapse-item"
                             to="404.html">404 Page
                </router-link>
                <router-link class="collapse-item"
                             to="blank.html">Blank Page
                </router-link>
            </div>
        </div>
    </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <router-link class="nav-link"
                     to="charts.html">
            <i class="fas fa-fw fa-chart-area">
            </i>
            <span>Charts</span>
        </router-link>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <router-link class="nav-link"
                     to="tables.html">
            <i class="fas fa-fw fa-table">
            </i>
            <span>Tables</span>
        </router-link>
    </li>

    <!-- Divider -->
    -
    <hr class="sidebar-divider d-none d-md-block">
    -

    <!-- Sidebar Toggler (Sidebar) -->
    <!--    <div class="text-center d-none d-md-inline">
            <button id="sidebarToggle"
                    class="rounded-circle border-0">
</button>
        </div>-->

    <!-- Sidebar Message -->
    <!--
    <div class="sidebar-card d-none d-lg-flex">
        <img alt="..."
             class="sidebar-card-illustration mb-2"
             src="img/undraw_rocket.svg">
        <p class="text-center mb-2">
<strong>SB Admin Pro</strong> is packed with premium features, components, and
                                                                  more!</p>
        <router-link class="btn btn-success btn-sm"
                     to="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!
        </router-link>
    </div>
    -->
    --}}
</ul>
