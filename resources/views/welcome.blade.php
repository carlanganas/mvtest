<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge"
          http-equiv="X-UA-Compatible">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="jwt-token" content="{{ \Auth::user()->getTokenFromUserObject() }}">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no"
          name="viewport">
    <meta content=""
          name="description">
    <meta content=""
          name="author">

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->

    <link
        href="{{url('css/fontawesome-free/css/all.min.css')}}"
        type="text/css"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <link
        href="{{url('css/app.css')}}"
        type="text/css"
        rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div id="app">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper"
             class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop"
                            class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    <form
                        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search d-none">
                        <!--
                        <div class="input-group">
                            <input aria-describedby="basic-addon2"
                                   aria-label="Search"
                                   class="form-control bg-light border-0 small"
                                   placeholder="Search for..."
                                   type="text">
                            <div class="input-group-append">
                                <button class="btn btn-primary"
                                        type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                        -->

                    </form>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a id="searchDropdown"
                               aria-expanded="false"
                               aria-haspopup="true"
                               class="nav-link dropdown-toggle"
                               data-toggle="dropdown"
                               href="#"
                               role="button">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div aria-labelledby="searchDropdown"
                                 class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input aria-describedby="basic-addon2"
                                               aria-label="Search"
                                               class="form-control bg-light border-0 small"
                                               placeholder="Search for..."
                                               type="text">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary"
                                                    type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <!-- Nav Item - Alerts -->
                        {{--
                        <li class="nav-item dropdown no-arrow mx-1">
                            <a id="alertsDropdown"
                               aria-expanded="false"
                               aria-haspopup="true"
                               class="nav-link dropdown-toggle"
                               data-toggle="dropdown"
                               href="#"
                               role="button">
                                <i class="fas fa-bell fa-fw"></i>
                                <!-- Counter - Alerts -->
                                <span class="badge badge-danger badge-counter">3+</span>
                            </a>
                            <!-- Dropdown - Alerts -->
                            <div aria-labelledby="alertsDropdown"
                                 class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in">
                                <h6 class="dropdown-header">
                                    Alerts Center
                                </h6>
                                <a class="dropdown-item d-flex align-items-center"
                                   href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-primary">
                                            <i class="fas fa-file-alt text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 12, 2019</div>
                                        <span class="font-weight-bold">A new monthly report is ready to download!</span>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center"
                                   href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-success">
                                            <i class="fas fa-donate text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 7, 2019</div>
                                        $290.29 has been deposited into your account!
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center"
                                   href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-warning">
                                            <i class="fas fa-exclamation-triangle text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 2, 2019</div>
                                        Spending Alert: We've noticed unusually high spending for your account.
                                    </div>
                                </a>
                                <a class="dropdown-item text-center small text-gray-500"
                                   href="#">Show All Alerts</a>
                            </div>
                        </li>
                        --}}

                        <!-- Nav Item - Messages -->
                        <message-component></message-component>

                        <div class="topbar-divider d-none d-sm-block"></div>


                        <user-component></user-component>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <router-view></router-view>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2021</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded"
       href="#app">
        <i class="fas fa-angle-up"></i>
    </a>


{{--
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <router-link href="&#45;&#45; url('/home') -&#45;&#45;">Home</router-link>
                @else
                    <router-link href="&#45;&#45; route('login') -&#45;&#45;">Login</router-link>

                    @if (Route::has('register'))
                        <router-link href="&#45;&#45; route('register') -&#45;&#45;">Register</router-link>
                    @endif
                @endauth
            </div>
        @endif

    </div>- --}}
</div>


</body>
<script
    src="{{url('js/app.js')}}"
></script>
<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('js/jquery.easing.min.js')}}"></script>
<script src="{{url('js/Chart.min.js')}}"></script>
<script src="{{url('js/sb-admin-2.min.js')}}"></script>


</html>
