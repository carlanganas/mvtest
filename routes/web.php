<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    use Illuminate\Support\Facades\Route;

    Route::get('/', function () {
        return redirect('/login');
    });

    Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::group()
    Route::middleware(['auth'])->prefix('home')->group(function () {

        Route::get('/{catchall?}', function () {
            return view('welcome');
        })->where('catchall', '(.*)');




    });
    Route::middleware(['jwt.verify'])->prefix('home')->group(function () {

        Route::post('/food', 'HomeController@getFood');
        Route::post('/user', 'HomeController@getUserData');
        Route::put('/cart', 'HomeController@saveCart');
        Route::post('/cart', 'HomeController@getCart');
    });

